package ru.t1.godyna.tm.dto.response.user;

import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.dto.response.AbstractUserResponse;
import ru.t1.godyna.tm.model.User;

public final class UserRemoveResponse extends AbstractUserResponse {

    public UserRemoveResponse(@Nullable final User user) {
        super(user);
    }

}
