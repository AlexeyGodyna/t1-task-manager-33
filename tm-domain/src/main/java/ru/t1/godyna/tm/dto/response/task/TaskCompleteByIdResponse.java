package ru.t1.godyna.tm.dto.response.task;

import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.model.Task;

public final class TaskCompleteByIdResponse extends AbstractTaskResponse {

    public TaskCompleteByIdResponse(@Nullable final Task task) {
        super(task);
    }

}
