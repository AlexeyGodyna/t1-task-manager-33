package ru.t1.godyna.tm.dto.response.project;

import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.model.Project;

public final class ProjectCreateResponse extends AbstractProjectResponse {

    public ProjectCreateResponse(@Nullable final Project project) {
        super(project);
    }

}
