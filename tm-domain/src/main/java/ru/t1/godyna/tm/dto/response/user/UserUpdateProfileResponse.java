package ru.t1.godyna.tm.dto.response.user;

import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.dto.response.AbstractUserResponse;
import ru.t1.godyna.tm.model.User;

public final class UserUpdateProfileResponse extends AbstractUserResponse {

    public UserUpdateProfileResponse(@Nullable final User user) {
        super(user);
    }

}
