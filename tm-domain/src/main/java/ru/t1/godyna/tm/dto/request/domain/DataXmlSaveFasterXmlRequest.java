package ru.t1.godyna.tm.dto.request.domain;

import lombok.NoArgsConstructor;
import ru.t1.godyna.tm.dto.request.AbstractUserRequest;

@NoArgsConstructor
public final class DataXmlSaveFasterXmlRequest extends AbstractUserRequest {
}
