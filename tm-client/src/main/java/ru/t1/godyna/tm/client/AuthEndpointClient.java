package ru.t1.godyna.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.godyna.tm.api.endpoint.IAuthEndpointClient;
import ru.t1.godyna.tm.dto.request.user.UserLoginRequest;
import ru.t1.godyna.tm.dto.request.user.UserLogoutRequest;
import ru.t1.godyna.tm.dto.request.user.UserProfileRequest;
import ru.t1.godyna.tm.dto.response.user.UserLoginResponse;
import ru.t1.godyna.tm.dto.response.user.UserLogoutResponse;
import ru.t1.godyna.tm.dto.response.user.UserProfileResponse;

@NoArgsConstructor
public final class AuthEndpointClient extends AbstractEndpointClient implements IAuthEndpointClient {

    @NotNull
    @Override
    public UserLoginResponse login(@NotNull final UserLoginRequest request) {
        return call(request, UserLoginResponse.class);
    }

    @NotNull
    @Override
    public UserLogoutResponse logout(@NotNull final UserLogoutRequest request) {
        return call(request, UserLogoutResponse.class);
    }

    @NotNull
    @Override
    public UserProfileResponse profile(@NotNull final UserProfileRequest request) {
        return call(request, UserProfileResponse.class);
    }

}
