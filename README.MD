# TASK MANAGER

## DEVELOPER INFO

**NAME**: Alexey Godyna

**EMAIL**: agodyna@t1-consulting.ru

**EMAIL**: alexey.godyna@gmail.com

## SOFTWARE

**JAVA**: JDK 1.8

**OS**: Windows 10 х64

## HARDWARE

**CPU**: i5

**RAM**: 8GB

**SSD**: 256GB

## BUID PROGRAM

```
mvn clean install
```

## RUN PROGRAM

```
java -jar ./task-manager.jar
```
